
// Calculator

var print = document.getElementById('print');
var erase = false;
var go = function(x) {
	if (x === 'ac') {
		/* implemetnasi clear all */
	} else if (x === 'eval') {
		print.value = Math.round(evil(print.value) * 10000) / 10000;
		erase = true;
	} else {
		print.value += x;
	}
};

function evil(fn) {
	return new Function('return ' + fn)();
}

// END

// Chat

/* ketik chat langsung keluar dan save ke localStorage */
chat.addEventListener("keyup", function(event) {
    event.preventDefault();
    if (event.keyCode === 13) {
      var existingchat = JSON.parse(localStorage.getItem("allchat"));
      if(existingchat == null) existingchat = [];
      localStorage.setItem("chat", chat.value);
      existingchat.push(chat.value);
      localStorage.setItem("allchat", JSON.stringify(existingchat));
      var para = document.createElement("p");
      para.classList.add("msg-send");
      var node = document.createTextNode(chat.value);
      para.appendChild(node);
      var element = document.getElementById("chat");
      element.appendChild(para);
      chat.value = '';
    }
});

/* load chat yang ada di localStorage biar tampil di chat*/
for (i = 0; i < existingchat.length; i++) {
  var para = document.createElement("p");
  para.classList.add("msg-send");
  var node = document.createTextNode(existingchat[i]);
  para.appendChild(node);
  var element = document.getElementById("chat");
  element.appendChild(para);
};

//END

// Theme

localStorage.setItem("theme", JSON.stringify([
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"},
    {"id":11,"text":"Default","bcgColor":"#95a5a6","fontColor":"#FAFAFA"}
]
));

var selectedTheme = JSON.parse(localStorage.getItem("selecttheme"));
if(selectedTheme == null) selectedTheme = {"id":11,"text":"Default","bcgColor":"#95a5a6","fontColor":"#FAFAFA"};
document.body.style.backgroundColor = selectedTheme['bcgColor'];

$(document).ready(function(){
    // kode jQuery selanjutnya akan ditulis disini
	$('.my-select').select2({
      'data': JSON.parse(localStorage.getItem("theme"))
      });
    $('.apply-button').click(function(){  // sesuaikan class button
      // [TODO] ambil value dari elemen select .my-select
      var temp = $(".my-select").val();
      var x = JSON.parse(localStorage.getItem("theme"));
      selectedTheme['bcgColor'] = x[temp]['bcgColor'];
      localStorage.setItem("selecttheme", JSON.stringify(x[temp]));
      document.body.style.backgroundColor = selectedTheme['bcgColor'];
	});
});

//END

